package WordFinder;

import WordFinder.WordFinder;
import org.assertj.core.api.Assertions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by mmaczka on 03.06.16.
 */
public class AlgorithmTest {

    @DataProvider
    public static Object[][] dataForPositiveResult() {
        return new Object[][] {{"someWord","somqWord"},{"qweewq","weewq"},{"zxccxz","zxccxx"},{"asomething","something"},{"somethinga","something"}};
    }


    @Test(dataProvider = "dataForPositiveResult")
    public void testAlgorithmForPositiveResults(String input,String inputWithTypo) throws Exception {
        //given


        //when
        boolean result= Algorithm.similarsWord(input,inputWithTypo);
        //then
        Assertions.assertThat(result).isTrue();
    }


    @DataProvider
    public static Object[][] dataForNegativeResult() {
        return new Object[][] {{"someWord","someeWord"}, {"longlonglong","short"},{"short","longlonglong"}};
    }

    @Test(dataProvider = "dataForNegativeResult")
    public void testAlgorithmForNegativeResults(String input,String inputWithTypo) throws Exception {
        //given


        //when
        boolean result= Algorithm.similarsWord(input,inputWithTypo);
        //then
        Assertions.assertThat(result).isFalse();
    }

}
