package controll;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import fileOperations.WordWriter;
import pojos.Index;
import pojos.SearchResult;
import pojos.Searchable;
import pojos.Word;
import pojos.XmlSearchResult;

@RestController
public class SearchResultController {

	Index counter = new Index();
	
	@RequestMapping(value = "/words/{word}/similar", method = RequestMethod.GET)
	public Searchable get(@RequestHeader("Accept") String accept, @PathVariable String word) {
		
		if(accept.equals("application/xml")) return getXml(word);
		return getJson(word);
	}

	private SearchResult getJson(String word) {
		SearchResult result = new SearchResult();
		result.search(word);
		return result;
	}
	
	private XmlSearchResult getXml(String word) {
		XmlSearchResult result = new XmlSearchResult(word);
		return result;
	}
	
	@RequestMapping(value = "/words", method = RequestMethod.POST)
	public ResponseEntity<Word> post(@RequestBody String jsonWord) {

		Word word = extractWord(jsonWord);
		ResponseEntity<Word> result = new ResponseEntity<>(word, HttpStatus.OK);
		WordWriter writer = new WordWriter();
		writer.write(counter.getIndex(), word.toString());
		return result;
	}

	private Word extractWord(String jsonWord) {
		Word word = createWordFromJson(jsonWord);
		return word;
	}

	public Word createWordFromJson(String jsonWord) {
		ObjectMapper mapper = new ObjectMapper();
		Word word = null;
		try {
			word = mapper.readValue(jsonWord, Word.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return word;
	}

}
