package WordFinder;

import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by mmaczka on 03.06.16.
 */
public class Main {
    public static void main(String[] args) throws URISyntaxException {
        WordFinder wordFinder=new WordFinder();
        List<String> result = wordFinder.find(args[0]);
        System.out.println(result);

    }
}
