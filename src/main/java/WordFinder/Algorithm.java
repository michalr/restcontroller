package WordFinder;

/**
 * Created by mmaczka on 03.06.16.
 */
public class Algorithm {
    public static boolean similarsWord(String inputedWord,String wordFromDatabase){

        if (inputedWord.matches("."+wordFromDatabase)) return true;
        if (inputedWord.matches(wordFromDatabase+".")) return true;


        int minimalNumberOfLoopIterations= Math.min(inputedWord.length(),wordFromDatabase.length()); //no reason to loop more than number of characters is shorter String

        for (int i = 1; i < minimalNumberOfLoopIterations+1 ; i++) {


            if (inputedWord.matches(wordFromDatabase.substring(0,i-1)+"."+wordFromDatabase.substring(i))) return true;
        }
        return false;
    }

}
