package WordFinder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by mmaczka on 03.06.16.
 */
public class WordFinder {


    String filePath = "src/main/resources/words.txt";


    public List<String> find(String inputWord) throws URISyntaxException {

        ArrayList<String> result= new ArrayList<>();


        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {

            stream.parallel().filter((e)-> Algorithm.similarsWord(inputWord,e)).forEach((e)->result.add(e));

        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}

