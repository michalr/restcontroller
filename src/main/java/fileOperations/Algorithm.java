package fileOperations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Algorithm {
	String path = "src/main/resources/words.txt";
	@Value("${M}")
	int M=2; // regex max length == M
	@Value("${N}")
	int N=1; // number of letters to cut

	public synchronized boolean isWordsMatches(String w1, String w2) {
	
		for (int i = 0; i < (w1.length() - N + 1); i++) {
			Pattern p = Pattern.compile(createPattern(w1, i));
			Matcher m = p.matcher(w2);

			if (m.matches())
				return true;
		}

		return false;
	}

	private String createPattern(String word, int i) {
		return word.substring(0, i) + "[a-z]{0," + M + "}" + word.substring(i + N);
	}

	public int getM() {
		return M;
	}

	public void setM(int m) {
		M = m;
	}

	public int getN() {
		return N;
	}

	public void setN(int n) {
		N = n;
	}

	
	
}
