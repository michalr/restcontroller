package fileOperations;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.testng.annotations.Test;

public class WordWriter {

	private static final String TARGET = "src/main/resources/appendedWords.txt";
	
	public void write(int index, String word) {
		
		try(PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(new File(TARGET), true)))){
			writer.println(index + "\t" + word);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testWrite() throws Exception {
		write(1, "aaron");
	}
}
