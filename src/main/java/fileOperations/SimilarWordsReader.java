package fileOperations;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import pojos.Word;

public class SimilarWordsReader {

	private static final String TARGET = "src/main/resources/appendedWords.txt";
	
	public List<Word> read(String word){
		
		List<Word> list = new ArrayList<>();
		Algorithm algorithm = new Algorithm();
		try(Scanner scanner = new Scanner(new File(TARGET))){
			while(scanner.hasNextLine()){
				String[] tab = scanner.nextLine().split("\\s+");
				if(algorithm.isWordsMatches(word, tab[1])){
					list.add(new Word(tab[1]));
				}
					
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
 		return list;
	}

	public static List<Word> readAll(){
		List<Word> list = new ArrayList<>();
		try(Scanner scanner = new Scanner(new File(TARGET))){
			while(scanner.hasNextLine()){
				String[] tab = scanner.nextLine().split("\\s+");
					list.add(new Word(tab[1]));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
}
