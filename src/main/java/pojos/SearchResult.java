package pojos;

import java.util.ArrayList;
import java.util.List;

import fileOperations.SimilarWordsReader;

public class SearchResult implements Searchable{

	private List<Word> words;

	public SearchResult() {
		words = new ArrayList<>();
	}

	public void search(String wordToSearch){
		SimilarWordsReader wordFinder = new SimilarWordsReader(); 
		words.addAll(wordFinder.read(wordToSearch));
		System.out.println(words);
	}
	
	public List<Word> getWords() {
		return words;
	}

	public void setWords(List<Word> words) {
		this.words = words;
	}

	@Override
	public String toString() {
		return ""+words;
	}
	
	
	
	
}
