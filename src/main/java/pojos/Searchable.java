package pojos;

import java.util.List;

public interface Searchable {

	List<Word> getWords();

}
