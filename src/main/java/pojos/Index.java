package pojos;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

import org.testng.annotations.Test;

public final class Index {

	private static AtomicInteger index;
	private final static String SOURCE = "src/main/resources/IndexCache.txt";

	public static int getIndex() {
		if (index == null) {
			readIndex();
		}

		int resultIndex = index.incrementAndGet();
		writeIndex(resultIndex);
		return resultIndex;
	}

	private static void readIndex() {
		try (Scanner scanner = new Scanner(new File(SOURCE))) {
			if (!scanner.hasNext())
				throw new IllegalStateException("There is no value in index cache");
			index = new AtomicInteger(scanner.nextInt());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void writeIndex(int resultIndex) {
		try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(new File(SOURCE), false)))) {
			writer.print(resultIndex);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testIndexGetting() {
		int index = Index.getIndex();
		System.out.println(index);
	}

}
