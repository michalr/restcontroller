package pojos;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class XmlSearchResult implements Searchable{

	private SearchResult searchResult;
	
	public XmlSearchResult() {
	}
	
	public XmlSearchResult(String word) {
		searchResult = new SearchResult();
		searchResult.search(word);
	}

	public SearchResult getSearchResult() {
		return searchResult;
	}

	public void setSearchResult(SearchResult searchResult) {
		this.searchResult = searchResult;
	}

	@Override
	public List<Word> getWords() {
		return searchResult.getWords();
	}
	
}
